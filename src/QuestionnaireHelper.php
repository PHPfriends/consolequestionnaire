<?php

namespace PHPfriends\ConsoleQuestionnaire;

use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Translation\Translator;

class QuestionnaireHelper
{
    /** @var OutputInterface */
    protected $output;
    /** @var InputInterface */
    protected $input;
    /** @var Translator */
    protected $translator;
    /** @var QuestionHelper */
    private $questionHelper;

    /**
     * @param OutputInterface $output
     * @param InputInterface  $input
     * @param Translator      $translator
     * @param QuestionHelper  $questionHelper
     */
    public function __construct(OutputInterface $output, InputInterface $input, QuestionHelper $questionHelper, Translator $translator = null)
    {
        $this->output = $output;
        $this->input = $input;
        $this->translator = $translator;
        $this->questionHelper = $questionHelper;
    }

    /**
     * @param Translator $translator
     */
    public function setTranslator($translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param $string
     *
     * @return string
     */
    protected function trans($string)
    {
        return $this->translator ? $this->translator->trans($string) : $string;
    }

    protected function drawLine()
    {
        $this->output->writeln("<bg=white>\n</>");
    }

    /**
     * @param array $questions
     * @param array $result
     *`
     *
     * @return array
     */
    public function printQuestions($questions, $result)
    {
        $valid = [];
        $this->drawLine();
        foreach ($questions as $questionId => $questionDetails) {
            $valid[] = $questionId;
            $questionKey = isset($questionDetails['key']) ?
                $questionDetails['key'] :
                $questionId;

            $this->output->writeln(
                sprintf('[<comment>%s</comment>] %s [<info>%s</info>]',
                    $questionId,
                    $this->trans($questionDetails['statement']),
                    isset($result[$questionKey]) ? $this->trans($result[$questionKey]) : ''
                )
            );
        }
        $this->output->write('[X] '.$this->trans('exit'));
        $this->drawLine();

        return $valid;
    }

    /**
     * @param string $questionId
     * @param mixed  $questions
     * @param mixed  $result
     */
    public function askQuestion($questionId, $questions, &$result)
    {
        $questionDetails = $questions[$questionId];
        $statement = $this->trans($questionDetails['statement']);
        $questionKey = isset($questionDetails['key']) ?
            $questionDetails['key'] :
            $questionId;
        if (!isset($result[$questionKey])) {
            $result[$questionKey] = null;
        }

        if (isset($questionDetails['options'])) {
            do {
                $valid = [];
                $selectedId = null;
                $this->output->writeln($statement.':');
                if (!in_array($result[$questionKey], $questionDetails['options'])) {
                    // get the first one
                    list($result[$questionKey]) = $questionDetails['options'];
                }
                foreach ($questionDetails['options'] as $opt => $option) {
                    $valid[] = $opt;
                    $selected = ($result[$questionKey] == $option);
                    if ($selected) {
                        $selectedId = $opt;
                    }
                    $this->output->writeln(sprintf(
                        '%s[%s] %s',
                        $selected ? '>>' : '  ',
                        $opt,
                        $this->trans($option)
                    ));
                }
                $question = new Question(sprintf('-> [%s] ? ', $selectedId), $selectedId);
                $selected = $this->questionHelper->ask($this->input, $this->output, $question);
            } while (!in_array($selected, $valid));
            $result[$questionKey] = $questionDetails['options'][$selected];
        } else {
            $question = new Question(
                sprintf(
                    '%s [<info>%s</info>] ? ',
                    $statement,
                    isset($result[$questionKey]) ? $result[$questionKey] : ''
                ),
                isset($result[$questionKey]) ? $result[$questionKey] :
                    (isset($questionDetails['default']) ? $questionDetails['default'] : null)
            );
            $result[$questionKey] = $this->questionHelper->ask($this->input, $this->output, $question);
        }
    }

    /**
     * @param mixed $questions
     * @param mixed $result
     */
    public function questionLoop($questions, &$result)
    {
        $output = $this->output;
        $input = $this->input;

        do {
            $valid = $this->printQuestions($questions, $result);
            $question = new Question($this->trans('select_option').' [X] ? ', 'X');
            $selected = $this->questionHelper->ask($input, $output, $question);

            if (in_array($selected, ['X', 'x'])) {
                break;
            }
            if (in_array($selected, $valid)) {
                $this->askQuestion($selected, $questions, $result);
            }
        } while (true);
    }
}
