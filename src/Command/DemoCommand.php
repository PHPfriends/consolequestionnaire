<?php

namespace PHPfriends\ConsoleQuestionnaire\Command;

use PHPfriends\ConsoleQuestionnaire\QuestionnaireHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DemoCommand extends AbstractBaseCommand
{
    protected function configure()
    {
        $this
            ->setName('demo')
            ->setDescription('Simple demo to serve you as a guide');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        parent::execute($input, $output);

        $helper = $this->getHelper('question');

        $questionnaire = new QuestionnaireHelper($output, $input, $helper);

        $data = [
            'name' => 'John Doe',
            'age' => '18-39',
            'sex' => 'no answer',
            'country' => 'Spain',
        ];

        $questions = [
            1 => [
                'key' => 'name',
                'statement' => 'What\'s your name',
            ],
            2 => [
                'key' => 'age',
                'statement' => 'What\'t your age range',
                'options' => [
                    '< 18',
                    '18-39',
                    '> 39',
                ],
            ],
            3 => [
                'key' => 'sex',
                'statement' => 'What\'s your sex',
                'options' => [
                    'female',
                    'male',
                    'no answer',
                ],
            ],
            4 => [
                'key' => 'country',
                'statement' => 'Where you come from',
                'options' => $this->getCountries(),
            ],
        ];


        $questionnaire->questionLoop($questions, $data);

        $output->writeln('Hello '.$data['name'].', you told me that you are in '.$data['age'].' age range and you come from '.$data['country']);
    }

    private function getCountries()
    {
        $result = [];
        $countries = json_decode(file_get_contents(__DIR__.'/../../countries.json'), true);
        foreach($countries as $country){
            $result[$country['code']] = $country['name'];
        }

        return $result;
    }
}
