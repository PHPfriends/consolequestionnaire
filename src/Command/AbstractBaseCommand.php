<?php

namespace PHPfriends\ConsoleQuestionnaire\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class AbstractBaseCommand extends Command
{
    /** @var OutputInterface */
    protected $output;
    /** @var InputInterface */
    protected $input;

    public function __construct($name = null)
    {
        parent::__construct($name);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->input = $input;
        $this->printLogo();
    }

    protected function printLogo()
    {
        $this->output->writeln(<<<EOD
<info>
,---.                 .      ,--.             .                              
|  -' ,-. ,-. ,-. ,-. |  ,-. |  | . . ,-. ,-. |- . ,-. ,-. ,-. ,-. . ,-. ,-. 
|  -. | | | | `-. | | |  |-' | \| | | |-' `-. |  | | | | | | | ,-| | |   |-' 
`---' `-' ' ' `-' `-' `' `-' `--\ `-' `-' `-' `' ' `-' ' ' ' ' `-^ ' '   `-' 
                                                                             
                                                                             

</info>
EOD
);
    }

    protected function drawLine()
    {
        $this->output->writeln("<bg=white>\n</>");
    }

    /**
     * @return \Twig_Environment
     */
    protected function getTwig()
    {
        return $this->getApplication()->getTwig();
    }
}
