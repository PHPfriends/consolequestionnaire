<?php

namespace PHPfriends\ConsoleQuestionnaire\Command;

use PHPfriends\ConsoleQuestionnaire\QuestionnaireHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Translation\Loader\ArrayLoader;
use Symfony\Component\Translation\Translator;

class AdvDemoCommand extends AbstractBaseCommand
{
    protected function configure()
    {
        $this
            ->setName('adv-demo')
            ->addOption('lang', null, InputOption::VALUE_REQUIRED, 'Language', 'en')
            ->setDescription('Advanced demo to serve you as a guide');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        parent::execute($input, $output);

        $language = $input->getOption('lang');
        if (!in_array($language, ['en', 'es'])) {
            throw new \Exception('Language not recognized');
        }
        $translator = new Translator($language);
        $translator->addLoader('array', new ArrayLoader());
        $translator->addResource('array', $this->getMessages($language), $language);

        $helper = $this->getHelper('question');

        $questionnaire = new QuestionnaireHelper($output, $input, $helper, $translator);

        $data = [
            'name' => 'John Doe',
            'age' => '18-39',
            'sex' => 'no answer',
            'country' => 'Spain',
        ];

        $questions = [
            1 => [
                'key' => 'name',
                'statement' => 'whats_your_name',
            ],
            2 => [
                'key' => 'age',
                'statement' => 'whats_your_age_range',
                'options' => [
                    '< 18',
                    '18-39',
                    '> 39',
                ],
            ],
            3 => [
                'key' => 'sex',
                'statement' => 'whats_your_sex',
                'options' => [
                    'female',
                    'male',
                    'no answer',
                ],
            ],
            4 => [
                'key' => 'country',
                'statement' => 'where_you_come_from',
                'options' => $this->getCountries(),
            ],
        ];

        $questionnaire->questionLoop($questions, $data);

        $output->writeln(sprintf($translator->trans('response'), $data['name'], $data['age'], $data['country']));
    }

    private function getCountries()
    {
        $result = [];
        $countries = json_decode(file_get_contents(__DIR__.'/../../countries.json'), true);
        foreach ($countries as $country) {
            $result[$country['code']] = $country['name'];
        }

        return $result;
    }

    private function getMessages($lang)
    {
        if ('es' == $lang) {
            return [
                'exit' => 'Salir',
                'select_option' => 'Elije una opción',
                'whats_your_name' => 'Cuál es tu nombre',
                'whats_your_age_range' => 'En qué edad estás',
                'whats_your_sex' => 'Qué eres',
                'where_you_come_from' => 'De dónde eres',
                'male' => 'Hombre',
                'female' => 'Mujer',
                'no answer' => 'No sabe',
                'response' => 'Hola %s, me has dicho que estás en el rango de %s de edad y que eres de %s',
            ];
        }

        // default language
        return [
            'exit' => 'Exit',
            'select_option' => 'Select one option',
            'whats_your_name' => 'What\'s your name',
            'whats_your_age_range' => 'What\'s your age range',
            'whats_your_sex' => 'What\'s your sex',
            'where_you_come_from' => 'Where you come from',
            'male' => 'Male',
            'female' => 'Female',
            'no answer' => 'No answer',
            'response' => 'Hello %s, you told me that you are in %s age range and you come from %s',
        ];
    }
}
